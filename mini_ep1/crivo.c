#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

int main()
{
  int input, N;
  int *crivo;
  int primos = 0;
  int primosEspeciais = 0;

  scanf("%d", &input);

  N = (1 << input);

  crivo = malloc((N + 1) * sizeof(int));

  for (int i = 2; i <= N; i++)
  {
    crivo[i] = TRUE;
  }

  for (int i = 2; i <= N; i++)
  {
    if (crivo[i])
    {
      primos = primos + 1;

      if (i % 4 != 3)
      {
        primosEspeciais = primosEspeciais + 1;
      }

      for (int j = i * 2; j <= N; j += i)
      {
        crivo[j] = FALSE;
      }
    }
  }

  printf("%d %d\n", primos, primosEspeciais);

  return 0;
}