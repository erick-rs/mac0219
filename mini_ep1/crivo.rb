#!/usr/bin/ruby

input = gets.chomp
N = 1 << input.to_i

crivo = Array.new(N + 1, true)
primos = 0
primosEspeciais = 0

for i in 2..N
  if crivo[i]
    primos += 1

    if i % 4 != 3
      primosEspeciais += 1
    end

    j = i * 2

    while j <= N
      crivo[j] = false
      j += i
    end
  end
end

puts "#{primos} #{primosEspeciais}"
