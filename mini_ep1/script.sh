#!/bin/bash

function statistics() {
    declare -A times
    sum=0
    size=0

    while read line; do
        times[$size]=$line
        sum=$((sum + line))
        size=$((size + 1))
    done

    average=$(bc <<< "scale=6;($sum / $size)")
    echo "Quantidade média de tempo de execução foi $average"

    variance=0

    for value in ${times[@]}; do
        variance=$(bc <<< "scale=6;( $variance + ($value - $average) * ($value - $average) )")
    done

    variance=$(bc <<< "scale=6;($variance / $size)")
    left=$(bc <<< "scale=6;( $average - 2.0003 * sqrt($variance / $size) )")
    right=$(bc <<< "scale=6;( $average + 2.0003 * sqrt($variance / $size) )")
    echo -e "Intervalo de confiança de 95% dos dados: [$left, $right]\n\n" 
}

# SCRIPT

gcc -o crivo crivo.c

for j in $(seq 2); do
    echo "ENTRADA ${j}:"

    echo "Linguagem Rápida:"

    for i in $(seq 5); do
        ts=$(date +%s%N)
        ./crivo < input${j}.txt
        tt=$((($(date +%s%N) - $ts)/1000000))
        echo ${tt} >> outputs1_${j}.txt
    done

    echo "Linguagem Lenta:"

    for i in $(seq 5); do
        ts=$(date +%s%N)
        ruby crivo.rb < input${j}.txt
        tt=$((($(date +%s%N) - $ts)/1000000))
        echo ${tt} >> outputs2_${j}.txt
    done

    echo -e "ARQUIVO input${j}.txt\n" >> final_output.txt

    echo "**** Linguagem Rápida ****" >> final_output.txt
    statistics < outputs1_${j}.txt >> final_output.txt

    echo "**** Linguagem Lenta ****" >> final_output.txt
    statistics < outputs2_${j}.txt >> final_output.txt
done
