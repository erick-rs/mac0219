#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

int main()
{
  int N, *crivo;
  int primos = 0;
  int primosEspeciais = 0;

  int _ = scanf("%d", &N);

  N = (1 << N);

  crivo = malloc((N / 2 + 1) * sizeof(int));
  memset(crivo, FALSE, (N / 2 + 1) * sizeof(int));

  for (int i = 1; i <= N / 2; i++)
    crivo[i] = TRUE;

  if (N >= 2)
  {
    primos++;
    primosEspeciais++;
  }

  for (int i = 3; i <= N; i += 2)
  {
    if (crivo[i / 2])
    {
      primos++;

      if (i % 4 != 3)
      {
        primosEspeciais++;
      }

      for (int j = i * 3; j <= N; j += i * 2)
        crivo[j / 2] = FALSE;
    }
  }

  printf("%d %d\n", primos, primosEspeciais);

  return 0;
}
