#!/usr/bin/ruby

input = gets.chomp
N = 1 << input.to_i

crivo = Array.new(N/2 + 1, true)
primos = 0
primos_especiais = 0

if N >= 2
  primos += 1
  primos_especiais += 1
end

i = 3

while i <= N
  if crivo[i / 2]
    primos += 1

    if i % 4 != 3
      primos_especiais += 1
    end

    j = i * 3
    while j <= N
      crivo[j / 2] = false
      j += i * 2
    end
  end

  i += 2
end

puts "#{primos} #{primos_especiais}"
