#!/bin/bash

function statistics() {
    declare -A times
    sum=0
    size=0

    while read line; do
        times[$size]=$line
        sum=$(bc <<< "scale=6;( $sum + $line )")
        size=$((size + 1))
    done

    average=$(bc <<< "scale=6;($sum / $size)")
    echo "Quantidade média de tempo de execução foi $average"

    variance=0

    for value in ${times[@]}; do
        variance=$(bc <<< "scale=6;( $variance + ($value - $average) * ($value - $average) )")
    done

    variance=$(bc <<< "scale=6;( $variance / ($size - 1) )")
    echo -e "A variância foi de $variance \n\n" 
}

function compile() {
    local ts=$(date +%s%N)
    make -B
    local tt=$(bc <<< "scale=6;( ($(date +%s%N) - $ts) / 1000000000 )")
    echo "Tempo de compilação: ${tt}" >> $1
}

function execute() {
    local ts=$(date +%s%N)
    ./src/lua bench.lua
    local tt=$(bc <<< "scale=6;( ($(date +%s%N) - $ts) / 1000000000 )")
    echo ${tt} >> testes.txt
}

function main() {
    compile $1

    for i in $(seq 1 5); do
        execute
    done

    statistics < testes.txt >> $1

    rm testes.txt
}

main $1
