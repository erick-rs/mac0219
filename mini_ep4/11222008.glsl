// Digite aqui seu código GLSL para gerar uma imagem
// O GLSL é muito próximo do C, mas não é idêntico.

// Essa linha vai definir a precisão do float,
// e mediump é bom o bastante no momento.
precision mediump float;

// Aqui não tem conversão automática entre int e float
// Então coloque .0 quando precisar de floats

// As janelas tem um tamanho fixo de 3000x3000
#define WIDTH (3000.0)
#define HEIGHT (3000.0)

// Constantes do limite do plano cartesiano
#define Xi (-2.0)
#define Xj (+1.0)
#define Yi (-1.5)
#define Yj (+1.5)

#define Xd (Xj - Xi)
#define Yd (Yj - Yi)

float interpolation(float A, float B, float percentage)
{
  return sqrt(A * A * (1.0 - percentage) + B * B * percentage);
}

// diferente do código em JS
// a GPU roda essa função main para cada pixel
void main()
{
  // Aqui o pixel é guardado num vetor de 4 floats
  // Também segue o formato RGBA, mas dessa vez
  // os campos variam de 0.0 a 1.0

  // vamos obter as coordenadas do plano a partir da
  // variável gl_FragCoord que é pré definida para cada pixel
  vec2 xy = (gl_FragCoord.xy / vec2(WIDTH, HEIGHT) * vec2(Xd, Yd)) + vec2(Xi, Yi);

  // Para acessa o x ou o y individualmente é só usar
  // xy.x e xy.y respectivamente
  float x = xy.x;
  float y = xy.y;

  //****************************
  float zx = 0.0, zy = 0.0;
  float red, green, blue;
  int steps = 0;

  for (int i = 0; i < 255; i++)
  {
    float zx_novo = zx * zx - zy * zy + x;
    float zy_novo = 2.0 * zx * zy + y;

    zx = zx_novo;
    zy = zy_novo;

    if (zx * zx + zy * zy > 4.0)
    {
      break;
    }

    steps += 1;
  }

  if (steps <= 10)
  {
    red = interpolation(0.0, 32.0, float(steps) / float(10));
    green = interpolation(7.0, 107.0, float(steps) / float(10));
    blue = interpolation(100.0, 203.0, float(steps) / float(10));
  }
  else if (steps <= 80)
  {
    red = interpolation(32.0, 237.0, float(steps - 10) / float(80 - 10));
    green = interpolation(107.0, 255.0, float(steps - 10) / float(80 - 10));
    blue = interpolation(203.0, 255.0, float(steps - 10) / float(80 - 10));
  }
  else if (steps <= 150)
  {
    red = interpolation(237.0, 255.0, float(steps - 80) / float(150 - 80));
    green = interpolation(255.0, 170.0, float(steps - 80) / float(150 - 80));
    blue = interpolation(255.0, 0.0, float(steps - 80) / float(150 - 80));
  }
  else if (steps <= 200)
  {
    red = interpolation(255.0, 100.0, float(steps - 150) / float(200 - 150));
    green = interpolation(170.0, 120.0, float(steps - 150) / float(200 - 150));
    blue = interpolation(0.0, 0.0, float(steps - 150) / float(200 - 150));
  }
  else if (steps <= 255)
  {
    red = interpolation(100.0, 0.0, float(steps - 200) / float(255 - 200));
    green = interpolation(120.0, 0.0, float(steps - 200) / float(255 - 200));
    blue = interpolation(0.0, 0.0, float(steps - 200) / float(255 - 200));
  }

  //*****************************
  // Aplica a cor
  gl_FragColor = vec4(red / float(255), green / float(255), blue / float(255), 1.0);
}

// Clique em Run GLSL para rodar o código ;)