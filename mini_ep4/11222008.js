// Digite aqui seu código JS para gerar uma imagem

// As janelas tem um tamanho fixo de 3000x3000
const WIDTH = 3000;
const HEIGHT = 3000;

// Constantes do limite o plano cartesiano
const Xi = -2.0;
const Xj = +1.0;
const Yi = -1.5;
const Yj = +1.5;

const Xd = Xj - Xi;
const Yd = Yj - Yi;

const colorInterval = Object.entries({
  10: { red: [0, 32], green: [7, 107], blue: [100, 203], percentage: (steps) => ((steps - 0) / (10 - 0)) },
  80: { red: [32, 237], green: [107, 255], blue: [203, 255], percentage: (steps) => ((steps - 10) / (80 - 10)) },
  150: { red: [237, 255], green: [255, 170], blue: [255, 0], percentage: (steps) => ((steps - 80) / (150 - 80)) },
  200: { red: [255, 100], green: [170, 120], blue: [0, 0], percentage: (steps) => ((steps - 150) / (200 - 150)) },
  255: { red: [100, 0], green: [120, 0], blue: [0, 0], percentage: (steps) => ((steps - 200) / (255 - 200)) },
});

const interpolation = (a, b, percentage) => (
  Math.sqrt(a * a * (1 - percentage) + b * b * percentage)
);

const main = (data) => {
  //Data é um array de bytes que representa a imagem
  //O valor mínimo é 0 e o máximo é 255
  //A cada 4 bytes temos um pixel
  //Red Green Blue Alpha

  //Vamos usar o for para percorrer cada pixel
  for (let i = 0; i < data.length; i += 4) {
    const _x = Math.floor((i/4) % 3000);
    const _y = Math.floor((i/4) / 3000);

    //converte para um plano cartesiano indo de Xi-Xj e Yi-Yj.
    const x = Xi + (_x/WIDTH) * Xd;
    const y = Yj - (_y/HEIGHT) * Yd;

    let zx = 0;
    let zy = 0;
    let steps = 0;

    while (steps < 255) {
      const zx_novo = zx ** 2 - zy ** 2 + x;
      const zy_novo = 2 * zx * zy + y;

      zx = zx_novo;
      zy = zy_novo;

      if (zx ** 2 + zy ** 2 > 4) {
        break;
      }

      steps += 1;
    }

    const values = colorInterval.find((obj) => steps <= parseInt(obj[0]))[1];
    const red = interpolation(values.red[0], values.red[1], values.percentage(steps));
    const green = interpolation(values.green[0], values.green[1], values.percentage(steps));
    const blue = interpolation(values.blue[0], values.blue[1], values.percentage(steps));
    
    //define a cor do pixel
    data[i]   = red;
    data[i+1] = green;
    data[i+2] = blue;
    data[i+3] = 255;
  }
};

// O nome da função a ser executada retornada deve aparecer no final
// clique em Run JS e aguarde um pouco para o seu código executar
main