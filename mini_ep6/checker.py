import sys
import subprocess
import random

def isSorted(v):
  n = len(v)
  for i in range(1, n):
    if v[i] < v[i - 1]:
      return False
  return True

def main():
  path = sys.argv[1]
  N = int(sys.argv[2])
  count_errors = 0

  for i in range(N):
    numbers = [str(n) for n in range(1, 101)]
    random.shuffle(numbers)
    args = [path] + numbers

    p = subprocess.Popen(args, stdout=subprocess.PIPE)
    (output, err) = p.communicate()
    p.wait()

    result = output.decode('utf-8').split(' ')
    result.pop()

    result = list(map(lambda x: int(x), result))

    if not isSorted(result):
      count_errors += 1

  if (count_errors / N) <= 0.1:
    print("Ok")
  else:
    print("Not Ok")


main()
