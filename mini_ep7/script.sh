#!/bin/bash

function calculate_average() {
    declare -A times
    sum=0
    size=0

    while read line; do
        times[$size]=$line
        sum=$(bc <<< "scale=6;($sum + $line)")
        size=$((size + 1))
    done

    average=$(bc <<< "scale=6;($sum / $size)")
    echo "Average: $average"
}

gcc parallelFor.c -o parallelFor -fopenmp

for i in $(seq 1 10); do
    { /usr/bin/time -f "%e" ./parallelFor ; } 2>> output.txt
done

calculate_average < output.txt >> $1
rm -r output.txt
